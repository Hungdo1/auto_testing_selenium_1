package javaTesster;

public class Topic_01_Data_Type {

	public static void main(String[] args) {
	 // Kiểu dữ liệu nguyên thủy 
		//char ký tự
		char a = 'u'; 
		//byte   (range 128 -127 )
		byte student = 127 ;
		
		//short  (range 256)
		short studenSum = 256 ;
	
		//int        (range 256)
		
		int mouseNumber = 2568934 ; 
		//long          (range 256)
		
		//float         (range 256)
		float employeeSalery = 30.5F ; 
		//double
		double  employeeSalarySum = 358.96D ; 
		//boolean  logic có 2 giá trị true/false
		
		boolean genderStatus = true;
		genderStatus = false; 
	// kiểu dữ liệu tham chiếu
		//string
				String address = "Ho CHi Minh" ;
		//array
		//class
		//object
		//collection

	}

}
